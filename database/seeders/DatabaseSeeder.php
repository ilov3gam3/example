<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->create([
            'name' => 'John Doe'
        ]);

        Post::factory(5)->create([
            'user_id' => $user->id
        ]);
        $user = User::factory()->create([
            'name' => 'Trần Quang Minh'
        ]);
        Post::factory(5)->create([
            'user_id' => $user->id
        ]);
        $user = User::factory()->create([
            'name' => 'Hoàng Anh Vũ'
        ]);
        Post::factory(5)->create([
            'user_id' => $user->id
        ]);
        DB::update("UPDATE posts
                            SET thumbnail =
                                CASE
                                    WHEN id = 1 THEN 'illustration-1.png'
                                    WHEN id = 2 THEN 'illustration-2.png'
                                    WHEN id = 3 THEN 'illustration-3.png'
                                    WHEN id = 4 THEN 'illustration-4.png'
                                    WHEN id = 5 THEN 'illustration-5.png'
                                    WHEN id = 6 THEN 'illustration-6.png'
                                    WHEN id = 7 THEN 'illustration-7.png'
                                    WHEN id = 8 THEN 'illustration-8.png'
                                    WHEN id = 9 THEN 'illustration-9.png'
                                    WHEN id = 10 THEN 'illustration-10.png'
                                    WHEN id = 11 THEN 'illustration-11.png'
                                    WHEN id = 12 THEN 'illustration-12.png'
                                    WHEN id = 13 THEN 'illustration-13.png'
                                    WHEN id = 14 THEN 'illustration-14.png'
                                    WHEN id = 15 THEN 'illustration-15.png'
                                    ELSE thumbnail
                                END;
                            ");
    }
}
